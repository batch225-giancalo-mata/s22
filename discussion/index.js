// Array Methods - Javascript has built-in functions and methods for arrays. This allows is to manipulate and access array items.

// Mutator Methods
/*
	- Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate the original array performing various tasks such as adding and removing elements.
	
*/


// push()

/*
	- Adds an element in the end of an array and return the array's length.

	Syntax:
		arrayName.push();

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method')
console.log(fruits);
//Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);


// pop()

/*
	- Removes the last element in an array and returns the removed element.

	-Syntax
		arrayName.pop();

*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method');
console.log(fruits);

// unshift()

/*
	- Adds one or more elements at the beginning of an array.

	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', elementB);

*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);


// shift() 

/* 
	- Removes an element at the beginning of an array and returns the removed element.

	- Syntax:

		arrayName.shift();

*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()

/*

	- Simultaneously removes elements from a specified index number and adds element.

	- Syntax:

		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/

let replaced1 = fruits.splice(1, 2, 'Lime', 'Cherry', 'Lemon'); 
console.log(replaced1);
console.log
('Mutated array from splice method'); console.log(fruits);

// sort()

/*

	-Rearrange the array elements in alphanumeric order.

	-Syntax:

		arrayName.sort()

*/
const randomThings = ["cat", "boy", "apps", "zoo"]

randomThings.sort();
console.log('Mutated array from sort method:');
console.log(randomThings);

// reverse()

/*
	- Reverses the order of array elements.

	- Syntax:

		arrayName.reverse();
*/

randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);

// Non-Mutator Methods

/*

	- Non-Mutator methods are functions that do not modify or change an array after they're created.
	- These metids do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output.

*/

// indexOf()

/*
	- Returns the index number of the first matching element found in an array.
	- if no match was found, the result will be -1
	- the search process will be done from the first element proceeding to the element.

	- Syntax:

		arrayName.indexOf(searchValue)

*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']

let firstIndex = countries.indexOf('CAN')
console.log('Result of indexOf method: ' + firstIndex);

// slice()

/*
	- Portion/slices elements from an array and returns a new array.
	
	- Syntax:

		arrayName.slice(startingIndex); 
		arrayName.slice(startingIndex, endingIndex); 
		
*/

// Slicing off elements from a specified index to the first element

let slicedArrayA = countries.slice(4);
console.log('Result from slice method');
console.log(slicedArrayA)
console.log(countries);

// Slicing off the elements from a specified index to another index

//Note: the last element is not included.

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method');
console.log(slicedArrayC)


// forEach();

/*

	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName must be plural for good practice and parameter should be singular.
	- Note: It must have function

	- Syntax:
		arrayName.forEach(function(indivElement)){
			statement
		});

*/

countries.forEach(function(country){
	console.log(country);
});


// includes()

/*

	includes()

	- includes() method checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
		- return true if the argument is found in the array.
		- return false if it is not.

		- Syntax:

			arrayName.includes(<argumentToFind>)

*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false